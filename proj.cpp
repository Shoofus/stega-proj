#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <string>
#include <cstddef>

/*TODO
 *-Need to rewrite the bit_stream function to not be a complete mess
 *-Need to add an interface of some kind, allow cover and payload files
 * to be input by the user rather than the hard coding I used for testing
 * and debugging
 *-Figure out why the bytes are being double on the final output extracted_bytes
 *-Add tests for edge cases and for general debugging(maybe toss em in a headerfile)
 */

using namespace std;

vector<unsigned char> get_file(string file){

	//Make the stream and open the file
	ifstream infile;//ifstream object
	infile.open(file, ios::binary);//open the file to read as bytes

	//Get the length for reasons
	infile.seekg(0, ios::end);//Set to last val in stream
	int length = infile.tellg();//Get length
	infile.seekg(0, ios::beg);//Set to beginning of stream
	//cout << length << endl;

	//Set positions
	ifstream::pos_type pos = infile.tellg();
	vector<unsigned char> result(pos);

	//read them bytes in
	while(infile){
		result.push_back(infile.get());
	}

	return result;

}

void write_file(vector<unsigned char> &stego_file){
	ofstream outfile;
	outfile.open("stego.bmp", ios::binary);

	for(int i = 0; i < stego_file.size(); i++)
		outfile << stego_file[i];

	outfile.seekp(0, ios::end);
	int out_length = outfile.tellp();
//	cout << out_length << endl;
	return;
}

int get_offset(vector<unsigned char> &vec){

//Function specific to .bmp files
	int result = 0;
	vector<unsigned char> buffer(4);

	buffer[0] = vec[13];
	buffer[1] = vec[12];
	buffer[2] = vec[11];
	buffer[3] = vec[10];

	result = int(	(unsigned char)(buffer[0]) << 24 |
			(unsigned char)(buffer[1]) << 16 |
			(unsigned char)(buffer[2]) << 8 |
			(unsigned char)(buffer[3]));
//	cout << result << endl;
	return result;
}

int max_payload_size(int cover_size, int cover_offset){
	//divide by 8 here due to encoding ONE bit PER byte
	int result = 0;
	result = (cover_size - cover_offset)/8;

	return result;
}

vector<unsigned char> size_encode(int size){
	vector<unsigned char> result;

	result.push_back((size >> 24) & 0xFF);
	result.push_back((size >> 16) & 0xFF);
	result.push_back((size >> 8) & 0xFF);
	result.push_back(size & 0xFF);

	return result;
}

//long long here due to the potential size(in bytes) of files
long long size_decode(vector<int> &vec){
	string bin_num = "";
	for(int i = 0; i < vec.size(); i++){
		if(vec[i] == 1)
			bin_num = bin_num + "1";
		else
			bin_num = bin_num + "0";
	}


	long long result = 0;
	int base = 1;
	int len = bin_num.length();
	for(int i = len -1; i >= 0; i--){
		if(bin_num[i] == '1')
			result = result + base;
		base = base * 2;
	}

	return result;
}

vector<int> bit_stream(vector<unsigned char> &vec){
	//TODO rewrite this whole function
	vector<int> result;
	int counter = vec.size();
	for(int i = 0; i < vec.size(); i++){
		int val = vec[i];
		if(val / 128 == 1){
			result.push_back(1);
			val = val - 128;
		} else {
			result.push_back(0);
		}
		if(val / 64 == 1){
			result.push_back(1);
			val = val - 64;
		} else {
			result.push_back(0);
		}
		if(val / 32 == 1){
			result.push_back(1);
			val = val - 32;
		} else {
			result.push_back(0);
		}
		if(val / 16 == 1){
			result.push_back(1);
			val = val - 16;
		} else {
			result.push_back(0);
		}
		if(val / 8 == 1){
			result.push_back(1);
			val = val - 8;
		} else {
			result.push_back(0);
		}
		if(val / 4 == 1){
			result.push_back(1);
			val = val - 4;
		} else {
			result.push_back(0);
		}
		if(val / 2 == 1){
			result.push_back(1);
			val = val - 2;
		} else {
			result.push_back(0);
		}
		if(val / 1 == 1){
			result.push_back(1);
			val = val - 1;
		} else {
			result.push_back(0);
		}
	}

	return result;
}

int last_bit(unsigned char byte){
	if(byte % 2 == 0)
		return 0;
	else
		return 1;
}

//All bytes end in 1 if odd, zero if even, we're using this here to encode
vector<unsigned char> encode_data(vector<unsigned char> &cover, vector<int> &payload_bit_stream, int data_offset){

	vector<unsigned char> result;
	int counter = 0;

	for(int i = 0; i < cover.size(); i++){
		if(i >= data_offset && i < data_offset + payload_bit_stream.size()){
			if(last_bit(cover[i]) == payload_bit_stream[counter]){
				counter++;
				result.push_back(cover[i]);
			} else {
				result.push_back(cover[i] + 1);
				counter++;
			}

		} else {
		result.push_back(cover[i]);
		}
	}

//	cout << result.size() << endl;
	return result;
}

unsigned int bin_string_to_int(string bin_string){
	unsigned int result = 0;
	int base = 1;
	int len = bin_string.length();
	for(int i = len - 1; i >= 0; i--){
		if(bin_string[i] == '1')
			result = result + base;
		base = base * 2;
	}

	return result;
}

vector<unsigned char> bin_to_bytes(vector<int> &pay_bits){
	vector<unsigned char> result;

	string bin_num = "";
	int counter = 0;
	for(int i = 0; i < pay_bits.size(); i += 8){
	counter = i;
		for(int j = counter; j < counter + 8; j++){
			if(pay_bits[j] == 1)
				bin_num = bin_num + "1";
			else
				bin_num = bin_num + "0";
		}

	unsigned int val = 0;
	val = bin_string_to_int(bin_num);
	bin_num = "";
	result.push_back(val);
	}

	return result;
}

vector<unsigned char> decode_data(vector<unsigned char> &stego_file, int data_offset){
	long long stego_len = 0;
	vector<int> bin_len;
	for(int i = data_offset; i < data_offset + 32; i++){
		bin_len.push_back(last_bit(stego_file[i]));
		cout << last_bit(stego_file[i]) << " ";
	}
	cout << endl;
	stego_len = size_decode(bin_len) * 8;
	cout << stego_len << endl;

	vector<int> pay_bits;
	for(int i = data_offset + 33; i < data_offset + stego_len + 32; i++){
		pay_bits.push_back(last_bit(stego_file[i]));
	}

	vector<unsigned char> result;
	result = bin_to_bytes(pay_bits);
	return result;
}

int main(){

	vector<unsigned char> cover;
	vector<unsigned char> payload;

	cover = get_file("mirri3.bmp");
	payload = get_file("text.txt");


	int cover_data_offset = 0;
	cover_data_offset = get_offset(cover);


	int pot_payload_size = 0;
	pot_payload_size = max_payload_size(cover.size(), cover_data_offset);


	vector<unsigned char> pay_len;
	pay_len = size_encode(payload.size());


//funky stuff going on here, this should not be permanent
	vector<int> payload_bits;

	payload_bits = bit_stream(pay_len);

	vector<int> temp_test;
	temp_test = bit_stream(payload);

	for(int i = 0; i < temp_test.size(); i++){
		payload_bits.push_back(temp_test[i]);
	}


	vector<unsigned char> stego_stream;
	stego_stream = encode_data(cover, payload_bits, cover_data_offset);


	write_file(stego_stream);


	vector<unsigned char> stego_file;
	stego_file = get_file("stego.bmp");


	int stego_offset = get_offset(stego_file);
	vector<unsigned char> extracted_bytes;
	extracted_bytes = decode_data(stego_file, stego_offset);

	//TODO
	//I have no idea (yet) why I'm doubling the bytes going in, definitely
	//need to fix this. BUT IT WORKS. 
	for(int i = 0; i < extracted_bytes.size(); i++)
		cout << (char)(extracted_bytes[i]/2) << endl;

}
